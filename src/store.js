import Vue from 'vue'
import Vuex from 'vuex'

import account from './modules/account'
import jobBrief from './modules/jobBrief'
import recruitment from './modules/recruitment'
import freelancer from './modules/freelancer'
import qualification from './modules/qualification'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    account,
    jobBrief,
    recruitment,
    freelancer,
    qualification,
  },
  state: {
    appName: 'Koralin',
    selection: {
      id: null,
      type: null
    },
    displayedName: 'untitled',
    imageUrls: {
      applicant: require('@/assets/progress-icons/applicant.png'),
      confirmed: require('@/assets/progress-icons/confirmed.png'),
      removed: require('@/assets/progress-icons/removed.png'),
      shortlisted: require('@/assets/progress-icons/shortlisted.png'),
      signed: require('@/assets/progress-icons/signed.png'),
      slot: require('@/assets/progress-icons/slot.png'),
      unknown: require('@/assets/progress-icons/unknown.png'),
    }
  },
  getters: {
    appName: (state) => state.appName,
    selection: (state) => state.selection,
    displayedName: (state, getters) => {
      let name = 'untitled'
      let details

      if (!state.selection.type) {
        return '-no selection-'
      } else {
        switch (state.selection.type) {
          case 'job-brief':
            details = getters['jobBrief/selected']
            break
          case 'recruitment':
            details = getters['recruitment/selected']
            break
          case 'freelancer':
            details = getters['freelancer/selected']
            break
        }
      }

      if (details) {
        name = details.name
      }

      return name
    },
    imageUrls: (state) => state.imageUrls,
  },
  mutations: {
    setSelection (state, value) {
      state.selection.id = value.id
      state.selection.type = value.type
    },
  },
  actions: {
    setSelection (context, value) {
      context.commit('setSelection', value)
    },
  }
})
