import Vue from 'vue'
import _ from 'lodash'
import utils from '../utils'

export default {
  namespaced: true,
  state: {
    data: {
      sample: {
        id: '',
        name: '',
        status: '',

        description: '',
        shortDescription: '',
        type: '',

        startDate: '',
        updatedAt: '',
        publishedAt: '',
        endDate: '',

        images: [
          {
            url: '',
            createdAt: '',
            updatedAt: '',
          }
        ],

        roles: {
          id: '',
          _order: '',
          name: '',
          needed: '',
          description: '',
          qualifications: {
            id: '',
            _order: '',
            level: '',
          },
          nPotential: '',
        }
      }
    }
  },
  getters: {
    dictionary: (state) => state.data,
    list: (state) => _.toArray(state.data),
    selected (state, getters, rootState) {
      const id = rootState.selection.id
      return state.data[id]
    },
  },
  mutations: {
    // UPDATING FROM DATABASE/SERVICE WORKER
    setAllData (state, data) {
      Vue.set(state, 'data', data)
    },
    setData (state, data) {
      _.forEach(data, item => {
        Vue.set(state.data, item.id, item)
      })
    },
  },
  actions: {
    // UPDATING FROM DATABASE/SERVICE WORKER
    /**
     * set the data of all items (replaces the whole thing)
     * from the database, or later, from service worker
     * @param  {Array} data the new data
     * @return {void}
     */
    setAllData (context, data) {
      context.commit('setAllData', utils.arrayToOrderedObject(data))
    },
    /**
     * set the data of the provided objects
     * from the database, or later, from service worker
     * @param  {Object} data the new data
     * @return {void}
     */
    setData (context, data) {
      context.commit('setData', data)
    },
  }
}
