import _ from 'lodash'

function dateToString (dateInt) {
  let date = new Date(dateInt)
  let string = 'undefined'
  if (typeof date === 'number') {
    date = new Date(date)
  }
  if (date) {
    string =
      date.getUTCFullYear() + '-' +
      (date.getMonth() < 10 ? '0' : '') +
      date.getMonth() + '-' +
      (date.getDate() < 10 ? '0' : '') +
      date.getDate()
  }
  return string
}

/**
 * replaces escaped linebreaks to actual linebreaks
 * @param  {[type]} headers [description]
 * @return {void}
 */
function wrapHeaders (headers) {
  _.forEach(headers, header => {
    header.innerHTML =
      header.innerHTML.replace(/&lt;\/*br\/*&gt;/gi, '<br/>')
  })
}

/**
 * converts an array with ids
 * to an object where each member
 * has "_order" value
 * @param  {Array}  array  the source array
 * @param  {String} idKey  the key to be used as id
 * @return {Object}       the generated object
 */
function arrayToOrderedObject (array, idKey = 'id') {
  const object = {}
  _.forEach(array, (member, idx) => {
    object[member[idKey]] = member
    object[member[idKey]]._order = idx
  })
  return object
}

/**
 * converts an object with members with
 * order to an array
 * @param  {Array}  object    the source object
 * @param  {String} orderKey  the key to be used as order
 * @return {Object}       the generated object
 */
function orderedObjectToArray (object, orderKey = '_order') {
  const array = []
  _.forEach(object, member => {
    array[member[orderKey]] = member
  })
  return array
}

function structureToDepthKeys (
  structure,
  depth = 0,
  depthKeys = []) {
  // init current depth
  depthKeys[depth] = []
  // for each structure field
  _.forEach(structure, (val, key) => {
    if (
      val === null ||
      typeof val === 'string'
    ) {
      // if not an object add the original key
      depthKeys[depth].push(key)
    } else {
      // else, dig deeper
      structureToDepthKeys(
        structure[key],
        depth + 1,
        depthKeys
      )
    }
  })
  return depthKeys
}

function convertToSubbedRows (rawRows, structure) {
  // collect current depth's keys
  // and next depth key
  const keys = []
  let nextKey
  _.forEach(structure, (val, key) => {
    if (val === null) {
      // if null, copy key to value and add
      structure[key] = key
      keys.push(key)
    } else if (typeof val === 'string') {
      // if string, also add
      keys.push(key)
    } else {
      // else, assign as the next dept key
      nextKey = key
    }
  })
  // if the rawRows is an object,
  // convert to an array
  if (!_.isArray(rawRows)) {
    const newRows = []
    _.forEach(rawRows, val => {
      newRows[val._order] = val
    })
    rawRows = newRows
  }
  // convert rows
  const rows = []
  _.forEach(rawRows, rawRow => {
    const newRow = {}
    rows.push(newRow)
    _.forEach(keys, key => {
      newRow[key] = rawRow[structure[key]]
    })
    // get next depth
    if (
      nextKey &&
      rawRow[nextKey] !== undefined
    ) {
      newRow._subs =
        convertToSubbedRows(
          rawRow[nextKey],
          structure[nextKey]
        )
    }
  })
  return rows
}

function explodeCompoundRows (subbedRows, depthKeys) {
  // function to recursively check each row
  function processRows (
    subbedRows,
    depth = 0,
    templateRow = {},
    rows = []) {
    // for each row
    let row
    subbedRows.map((subbedRow, idx) => {
      row = _.cloneDeep(templateRow)
      // for each field, copy value
      _.forEach(depthKeys[depth], key => {
        row[key] = subbedRow[key]
      })
      // if there are no subs, add to rows
      if (
        !subbedRow._subs ||
        subbedRow._subs.length === 0
      ) {
        rows.push(row)
      } else {
        // if there are subs, create sub rows
        processRows(
          subbedRow._subs,
          depth + 1,
          row,
          rows
        )
      }
    })
    return rows
  }
  return processRows(subbedRows)
}

function setRowDepths (rows, depthKeys) {
  // then process each row
  rows.map((row, idx) => {
    // if first row, assign depth 0
    if (idx === 0) {
      row._depth = 0
    } else {
      // else find the first difference of value
      // and record the depth of the key
      const prevRow = rows[idx - 1]
      _.forEach(depthKeys, (keys, depth) => {
        _.forEach(keys, key => {
          if (row[key] !== prevRow[key]) {
            row._depth = depth
            return false
          }
        })
        if (row._depth !== undefined) {
          return false
        }
      })
    }
  })
  return rows
}

export default {
  dateToString,
  wrapHeaders,

  arrayToOrderedObject,
  orderedObjectToArray,

  structureToDepthKeys,
  convertToSubbedRows,
  explodeCompoundRows,
  setRowDepths,
}
