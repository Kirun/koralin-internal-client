import Vue from 'vue'
import Router from 'vue-router'

import JobBrowser from '@/components/JobBrowser'
import JobDetails from '@/components/JobDetails'
import FreelancerBrowser from '@/components/FreelancerBrowser'
import Freelancer from '@/components/Freelancer'

Vue.use(Router)

const defaultPage = '/jobs/job-brief'

const routes = [
  // job browsing
  {
    path: '/jobs/:type',
    name: 'Job Browser',
    props: true,
    component: JobBrowser,
  },
  {
    path: '/jobs',
    redirect: '/jobs/job-brief'
  },
  // job details
  {
    path: '/job/:type/:id',
    name: 'Job Details',
    props: true,
    component: JobDetails,
  },
  // freelancer
  {
    path: '/freelancers',
    name: 'Freelancer Browser',
    component: FreelancerBrowser
  },
  {
    path: '/freelancer/:id',
    name: 'Freelancer',
    props: true,
    component: Freelancer
  },
  {
    path: '*',
    redirect: defaultPage,
  },
]

const router = new Router({
  routes
})

export default router
