// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueSocketio from 'vue-socket.io'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(VueSocketio, 'http://128.199.101.69:3000')
// Vue.use(VueSocketio, 'localhost:3000')
Vue.use(Vuetify, {
  theme: {
    primary: '#1DE9B6',
    secondary: '#212121',
    accent: '#FF6D00',
    error: '#D50000',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#009688',
  },
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
